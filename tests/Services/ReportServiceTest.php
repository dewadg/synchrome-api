<?php

use App\Services\ReportService;
use Faker\Factory;
use Illuminate\Support\Collection;
use Laravel\Lumen\Testing\DatabaseMigrations;

class ReportServiceTest extends TestCase
{
    use DatabaseMigrations;

    protected $faker;

    /**
     * @var ReportService
     */
    protected $test_report_service;

    public function setUp()
    {
        parent::setUp();

        $seeder = app(DatabaseSeeder::class);

        $seeder->run();
        $seeder->call('DevAttendancesTableSeeder');

        $this->faker = Factory::create();
        $this->test_report_service = new ReportService;
    }

    public function testGetTppReportForAgency()
    {
        $data = [
            'agency_id' => '4.06',
            'start' => '2019-07-01',
            'end' => '2019-07-31',
        ];

        $actual = $this->test_report_service->getTppReportForAgency($data);

        $this->assertInstanceOf(Collection::class, $actual);
    }

    public function testGetTppReportForAsn()
    {
        $data = [
            'asn_id' => '199709152017011001',
            'start' => '2019-07-01',
            'end' => '2019-07-31',
        ];

        $actual = $this->test_report_service->getTppReportForAsn($data);
        
        var_dump($actual);

        $this->assertInstanceOf(Collection::class, $actual);
    }
}
