<?php

namespace App\Services;

use App\Asn;
use App\Attendance;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ReportService
{
    /**
     * Generate attendance report data for an agency
     *
     * @param array $data
     * @return Collection
     */
    public function getTppReportForAgency(array $data)
    {
        try {
            $start = Carbon::parse($data['start']);
            $end = Carbon::parse($data['end']);

            $latest_attendance_data = Attendance::whereHas('asn', function ($query) use ($data) {
                $query->where('agency_id', $data['agency_id']);
            })->orderBy('date', 'desc')
                ->first();

            if ($latest_attendance_data) {
                $date = Carbon::parse($latest_attendance_data->date);

                $end = $date->lt($end) ? $date : $end;
            }

            $asn_list = Asn::where('agency_id', $data['agency_id'])
                ->with([
                    'agency',
                    'rank',
                    'echelon',
                    'tpp',
                    'workshift',
                    'workshift.details',
                    'calendar',
                    'fingerprints',
                ])
                ->get();

            $attendance_map = Attendance::whereBetween('date', [$data['start'], $data['end']])
                ->with([
                    'type',
                    'asn',
                    'asn.agency',
                ])
                ->get()
                ->filter(function ($item) use ($data) {
                    return ($item->asn->agency->id === $data['agency_id']) &&
                        $item->type->tpp_paid;
                })
                ->reduce(function ($carry, $item) {
                    $carry[$item->asn_id] = isset($carry[$item->asn_id])
                        ? array_merge($carry[$item->asn_id], [$item])
                        : [$item];

                    return $carry;
                }, []);

            return $asn_list->map(function ($asn) use (
                $attendance_map,
                $start,
                $end
            ) {
                $attendances = isset($attendance_map[$asn->id]) ? $attendance_map[$asn->id] : null;
                $initial_tpp = $asn->tpp->value;

                if (! $attendances) {
                    return [
                        'asn' => $asn,
                        'tpp' => [
                            'initial_tpp' => $initial_tpp,
                            'final_tpp' => 0,
                            'attendance_rate' => 0,
                        ],
                    ];
                }

                $expected_total_work_duration = $asn->getTotalWorkDurationBetween(
                    $start->format('Y-m-d'),
                    $end->format('Y-m-d')
                );

                $actual_total_work_duration = collect($attendances)
                    ->filter(function ($item) {
                        return $item->type->tpp_paid;
                    })
                    ->reduce(function ($carry, $item) use ($asn) {
                        $today = date('Y-m-d');

                        if ($item->type->id === 'L') {
                            return $carry + $asn->getTotalWorkDurationBetween(
                                $today,
                                $today
                            );
                        }

                        $check_in = Carbon::parse($today . ' ' . $item->check_in);
                        $check_out = Carbon::parse($today . ' ' . $item->check_out);

                        return $carry + ($check_out->diffInMinutes($check_in));
                    }, 0);

                $attendance_rate = $actual_total_work_duration / $expected_total_work_duration;
                $final_tpp = $attendance_rate * $initial_tpp;

                return [
                    'asn' => $asn,
                    'tpp' => [
                        'initial_tpp' => $initial_tpp,
                        'final_tpp' => $final_tpp,
                        'attendance_rate' => $attendance_rate * 100,
                    ],
                ];
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Get TPP report of an ASN.
     *
     * @param array $data
     * @return Collection
     */
    public function getTppReportForAsn(array $data)
    {
        try {
            $start = Carbon::parse($data['start']);
            $end = Carbon::parse($data['end']);

            $latest_attendance_data = Attendance::whereHas('asn', function ($query) use ($data) {
                $query->where([
                    'asn_id' => $data['asn_id'],
                ]);
            })->orderBy('date', 'desc')
                ->first();

            if ($latest_attendance_data) {
                $date = Carbon::parse($latest_attendance_data->date);

                $end = $date->lt($end) ? $date : $end;
            }

            $asn = Asn::with([
                'agency',
                'rank',
                'echelon',
                'tpp',
                'workshift',
                'calendar',
                'fingerprints',
            ])->findOrFail($data['asn_id']);

            $attendances = Attendance::whereBetween('date', [$data['start'], $data['end']])
                ->where('asn_id', $asn->id)
                ->with([
                    'type',
                ])
                ->orderBy('date', 'asc')
                ->get()
                ->reduce(function ($carry, $item) {
                    $carry[$item->date] = $item;

                    return $carry;
                }, []);

            $expected_total_work_duration = $asn->getTotalWorkDurationBetween(
                $start->format('Y-m-d'),
                $end->format('Y-m-d')
            );

            $report = collect([]);

            for ($i = $start; $i->lte($end); $i->addDay()) {
                if (! isset($attendances[$i->format('Y-m-d')])) {
                    $report->push([
                        'date' => $i->format('Y-m-d'),
                        'is_workday_according_to_workshift' => false,
                        'check_in' => null,
                        'check_out' => null,
                        'deduction' => 0,
                        'info' => null,
                        'type' => null,
                    ]);

                    continue;
                }

                $attendance_data = $attendances[$i->format('Y-m-d')];

                $expected_work_duration = $asn->getTotalWorkDurationBetween(
                    $i->format('Y-m-d'),
                    $i->format('Y-m-d')
                );

                $actual_check_in = Carbon::parse($attendance_data->date . ' ' . $attendance_data->check_in);
                $actual_check_out = Carbon::parse($attendance_data->date . ' ' . $attendance_data->check_out);
                $actual_work_duration = $attendance_data->type->id === 'L'
                    ? $expected_work_duration
                    : $actual_check_out->diffInMinutes($actual_check_in);

                $non_work_duration = $expected_work_duration - $actual_work_duration;
                $deduction = ! $attendance_data->type->tpp_paid
                    ? ($expected_work_duration / $expected_total_work_duration) * $asn->tpp->value
                    : ($non_work_duration / $expected_total_work_duration) * $asn->tpp->value;

                $report->push([
                    'date' => $i->format('Y-m-d'),
                    'is_workday_according_to_workshift' => true,
                    'check_in' => $attendance_data->check_in,
                    'check_out' => $attendance_data->check_out,
                    'deduction' => $deduction,
                    'info' => $attendance_data->info,
                    'type' => $attendance_data->type,
                ]);
            }

            return $report;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
