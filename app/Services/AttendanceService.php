<?php

namespace App\Services;

use App\Attendance;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class AttendanceService
{
    /**
     * Get attendances by date range
     *
     * @return Collection
     */
    public function get($start, $end)
    {
        $includes = [
            'asn',
            'asn.calendar',
            'asn.calendar.events',
            'asn.calendar.events.attendanceType',
            'asn.workshift',
            'asn.workshift.details',
            'attendanceType',
        ];

        return Attendance::with($includes)
            ->whereBetween('date', [$start, $end])
            ->get();
    }

    /**
     * Sync attendances
     *
     * @param array $data
     * @return void
     */
    public function sync(array $data)
    {
        try {
            DB::transaction(function () use ($data) {
                collect($data)->each(function ($item) {
                    $found = Attendance::where([
                        'date' => $item['date'],
                        'asn_id' => $item['asn_id'],
                    ])->first();

                    if ($found) {
                        return true;
                    }

                    Attendance::create([
                        'asn_id' => $item['asn_id'],
                        'date' => $item['date'],
                        'check_in' => $item['check_in'],
                        'check_out' => $item['check_out'],
                        'delay' => $item['delay'],
                        'attendance_type_id' => $item['attendance_type_id'],
                        'info' => $item['info'],
                    ]);
                });
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
