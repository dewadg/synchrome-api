<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Attendance
 * @package App
 *
 * @SWG\Definition(
 *     definition="AttendanceSyncRequest",
 *     @SWG\Property(
 *          property="attendances",
 *          type="array",
 *          @SWG\Items(
 *              @SWG\Property(
 *                  property="asnId",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="date",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="checkIn",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="checkOut",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="delay",
 *                  type="number"
 *              ),
 *              @SWG\Property(
 *                  property="attendanceTypeId",
 *                  type="string"
 *              ),
 *              @SWG\Property(
 *                  property="info",
 *                  type="string"
 *              )
 *          )
 *     )
 * )
 */
class Attendance extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'date',
        'asn_id',
        'check_in',
        'check_out',
        'delay',
        'attendance_type_id',
        'info',
    ];

    /**
     * This attendance's ASN
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function asn()
    {
        return $this->belongsTo(Asn::class);
    }

    /**
     * This attendance's type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(AttendanceType::class, 'attendance_type_id');
    }
}
