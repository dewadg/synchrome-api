<?php

namespace App\Transformers;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class AgencyTppReportTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = [
        'asn',
    ];

    /**
     * Transform the data.
     *
     * @param array $data
     * @return array
     */
    public function transform(array $data)
    {
        return [
            'initialTpp' => $data['tpp']['initial_tpp'],
            'finalTpp' => $data['tpp']['final_tpp'],
            'attendanceRate' => $data['tpp']['attendance_rate'],
        ];
    }

    /**
     * Include ASN data
     *
     * @param array $data
     * @return Item
     */
    public function includeAsn(array $data)
    {
        return $this->item($data['asn'], new AsnTransformer);
    }
}
