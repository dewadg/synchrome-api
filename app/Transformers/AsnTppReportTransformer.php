<?php

namespace App\Transformers;

use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class AsnTppReportTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $defaultIncludes = [
        'type',
    ];

    /**
     * Transform the data.
     *
     * @param array $data
     * @return array
     */
    public function transform(array $data)
    {
        return [
            'date' => $data['date'],
            'isWorkdayAccordingToWorkshift' => $data['is_workday_according_to_workshift'],
            'checkIn' => $data['check_in'],
            'checkOut' => $data['check_out'],
            'deduction' => $data['deduction'],
            'info' => $data['info'],
        ];
    }

    /**
     * Include attendance type data
     *
     * @param array $data
     * @return Item
     */
    public function includeType(array $data)
    {
        return $data['type']
            ? $this->item($data['type'], new AttendanceTypeTransformer)
            : null;
    }
}
