<?php

namespace App\Http\Controllers;

use App\Services\AttendanceService;
use Illuminate\Http\Request;

class AttendanceController extends RestController
{
    /**
     * @var AttendanceService
     */
    protected $attendance_service;

    /**
     * AttendanceController constructor
     *
     * @param AttendanceService $attendance_service
     */
    public function __construct(AttendanceService $attendance_service)
    {
        $this->attendance_service = $attendance_service;
    }

    /**
     * @SWG\Post(
     *     path="/attendances",
     *     tags={"Attendance"},
     *     operationId="attendancesIndex",
     *     summary="Store/sync attendances.",
     *     security={{"basicAuth":{}}},
     *     @SWG\Parameter(
     *         name="params",
     *         in="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/AttendanceSyncRequest")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success."
     *     )
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sync(Request $request)
    {
        $this->validate($request, [
            'attendances' => 'required|array',
            'attendances.*.asnId' => 'required',
            'attendances.*.date' => 'required|date',
            'attendances.*.checkIn' => 'present',
            'attendances.*.checkOut' => 'present',
            'attendances.*.delay' => 'present',
            'attendances.*.attendanceTypeId' => 'required',
            'attendances.*.info' => 'present',
        ]);

        try {
            $data = collect($request->input('attendances'))
                ->map(function ($item) {
                    return [
                        'asn_id' => $item['asnId'],
                        'date' => $item['date'],
                        'check_in' => $item['checkIn'],
                        'check_out' => $item['checkOut'],
                        'delay' => $item['delay'],
                        'attendance_type_id' => $item['attendanceTypeId'],
                        'info' => $item['info'],
                    ];
                })
                ->all();

            $this->attendance_service->sync($data);

            return response()->json();
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }
}
