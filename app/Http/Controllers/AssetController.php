<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AssetController extends Controller
{
    /**
     * @param string $file_name
     * @return BinaryFileResponse
     */
    public function serveImg($file_name)
    {
        try {
            $mime_type = Storage::mimeType('img/' . $file_name);
            $content =  Storage::get('img/' . $file_name);

            return response($content, 200, ['Content-Type' => $mime_type]);
        } catch (\Exception $e) {
            abort(404);
        }
    }
}
