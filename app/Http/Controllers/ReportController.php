<?php

namespace App\Http\Controllers;

use App\AttendanceType;
use App\Services\AgencyService;
use App\Services\AsnService;
use App\Services\ReportService;
use App\Transformers\AgencyTppReportTransformer;
use App\Transformers\AsnTppReportTransformer;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ReportController extends RestController
{
    /**
     * @var ReportService
     */
    protected $report_service;

    /**
     * @var AsnService
     */
    protected $asn_service;

    /**
     * @var AgencyService
     */
    protected $agency_service;

    /**
     * ReportController constructor
     *
     * @param ReportService $report_service
     */
    public function __construct(
        ReportService $report_service,
        AsnService $asn_service,
        AgencyService $agency_service
    ) {
        parent::__construct();

        $this->report_service = $report_service;
        $this->asn_service = $asn_service;
        $this->agency_service = $agency_service;
    }

    /**
     * @SWG\Get(
     *     path="/report/tpp-by-agency",
     *     tags={"Report"},
     *     operationId="reportAgencyTpp",
     *     summary="Generate TPP report for an agency.",
     *     security={{"basicAuth":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="TPP report."
     *     )
     * )
     *
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function tppReportForAgency(Request $request)
    {
        $this->validate($request, [
            'start' => 'required|date',
            'end' => 'required|date',
            'agencyId' => 'required',
        ]);

        try {
            $report_data = $this->report_service
                ->getTppReportForAgency([
                    'start' => $request->input('start'),
                    'end' => $request->input('end'),
                    'agency_id' => $request->input('agencyId'),
                ])
                ->all();

            return $this->sendCollection($report_data, AgencyTppReportTransformer::class);
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * Export TPP report for Agency
     *
     * @param Request $request
     * @return Symfony\Component\HttpFoundation\BinaryFileResponse;
     */
    public function exportTppReportForAgency(Request $request)
    {
        $this->validate($request, [
            'start' => 'required|date',
            'end' => 'required|date',
            'agencyId' => 'required',
        ]);

        try {
            $start = $request->input('start');
            $end = $request->input('end');
            $agency = $this->agency_service->find($request->input('agencyId'));

            $report_data = $this->report_service
                ->getTppReportForAgency([
                    'start' => $request->input('start'),
                    'end' => $request->input('end'),
                    'agency_id' => $request->input('agencyId'),
                ])
                ->map(function ($item) {
                    $item['tpp']['deduction'] = $item['tpp']['initial_tpp'] - $item['tpp']['final_tpp'];

                    $item['tpp']['initial_tpp'] = 'Rp' . number_format(
                        $item['tpp']['initial_tpp'],
                        0,
                        '',
                        '.'
                    ) . ',00';

                    $item['tpp']['deduction'] = 'Rp' . number_format(
                        $item['tpp']['deduction'],
                        0,
                        '',
                        '.'
                    ) . ',00';

                    $item['tpp']['final_tpp'] = 'Rp' . number_format(
                        $item['tpp']['final_tpp'],
                        0,
                        '',
                        '.'
                    ) . ',00';

                    $item['tpp']['attendance_rate'] = number_format(
                        $item['tpp']['attendance_rate'],
                        0
                    ) . '%';

                    return $item;
                })
                ->all();

            $pdf = PDF::loadView('pdf.tpp-agency', compact(
                'start',
                'end',
                'agency',
                'report_data'
            ))->setPaper('a4', 'landscape');

            return $pdf->stream();
        } catch (\Exception $e) {
            throw $e;
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * @SWG\Get(
     *     path="/report/tpp-by-asn",
     *     tags={"Report"},
     *     operationId="reportAsnTpp",
     *     summary="Generate TPP report for an ASN.",
     *     security={{"basicAuth":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="TPP report."
     *     )
     * )
     *
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function tppReportForAsn(Request $request)
    {
        $this->validate($request, [
            'start' => 'required|date',
            'end' => 'required|date',
            'asnId' => 'required',
        ]);

        try {
            $report_data = $this->report_service
                ->getTppReportForAsn([
                    'start' => $request->input('start'),
                    'end' => $request->input('end'),
                    'asn_id' => $request->input('asnId'),
                ])
                ->all();

            return $this->sendCollection($report_data, AsnTppReportTransformer::class);
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * Export TPP report for ASN
     *
     * @param Request $request
     * @return Symfony\Component\HttpFoundation\BinaryFileResponse;
     */
    public function exportTppReportForAsn(Request $request)
    {
        $this->validate($request, [
            'start' => 'required|date',
            'end' => 'required|date',
            'asnId' => 'required',
        ]);

        try {
            $start = $request->input('start');
            $end = $request->input('end');
            $asn = $this->asn_service->find($request->input('asnId'));
            $initial_tpp = $asn->tpp->value;
            $total_deduction = 0;
            $final_tpp = 0;

            $attendance_types = AttendanceType::get()
                ->filter(function ($item) {
                    return is_null($item->parent_attendance_type_id);
                })
                ->reduce(function ($carry, $item) {
                    if (! $carry) {
                        return $item->id . ' = ' . $item->name;
                    }

                    return $carry . ', ' . $item->id . ' = ' . $item->name;
                }, '');

            $report_data = $this->report_service
                ->getTppReportForAsn([
                    'start' => $start,
                    'end' => $end,
                    'asn_id' => $request->input('asnId'),
                ])
                ->filter(function ($item) {
                    return $item['is_workday_according_to_workshift'];
                })
                ->map(function ($item) use (&$total_deduction) {
                    $total_deduction += $item['deduction'];

                    $item['deduction'] = 'Rp' . number_format($item['deduction'], 0, '', '.') . ',00';

                    return $item;
                })
                ->all();

            $final_tpp = 'Rp' . number_format(($initial_tpp - $total_deduction), 0, '', '.') . ',00';
            $total_deduction = 'Rp' . number_format($total_deduction, 0, '', '.') . ',00';
            $initial_tpp = 'Rp' . number_format($initial_tpp, 0, '', '.') . ',00';

            $pdf = PDF::loadView('pdf.tpp-asn', compact(
                'start',
                'end',
                'asn',
                'initial_tpp',
                'total_deduction',
                'final_tpp',
                'report_data',
                'attendance_types'
            ));

            return $pdf->stream();
        } catch (ModelNotFoundException $e) {
            return $this->notFoundResponse('ASN not found');
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }
}
