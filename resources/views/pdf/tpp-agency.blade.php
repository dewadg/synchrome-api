<!DOCTYPE html>
<html>
  <head>
    <title>Laporan TPP OPD</title>
    <style>
      body {
        font-family: sans-serif;
        font-size: 10pt;
      }

      header {
        border-bottom: 2px solid #000;
        margin-bottom: 15px;
        padding-bottom: 10px;
      }

      header img {
        height: 100px;
      }

      header h1 {
        font-size: 1.6rem;
        line-height: 1.6rem;
        margin: 0 0 5px 0;
        text-transform: uppercase;
      }

      header p {
        margin: 0 0 5px 0;
      }

      table.report {
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
        margin-bottom: 25px;
      }

      table.report thead tr th,
      table.report tbody tr td {
        border: 1px solid #000;
        padding: 2.5px;
      }

      .footer p {
        margin-bottom: -5px;
      }

      .text-center {
        text-align: center;
      }

      .text-right {
        text-align: right;
      }

      .summary {
        padding: 5px !important;
      }
    </style>
  </head>
  <body>
    <header>
      <table width="100%">
        <tbody>
          <tr>
            <td width="1%">
              <img src="{{ resource_path('/img/bali.png') }}">
            </td>
            <td>
              <h1 align="center">Pemerintah Provinsi Bali</h1>
              <h1 align="center">{{ $agency->name }}</h1>
              <p align="center">
                {{ $agency->address }}
              </p>
              <p align="center">
                {{ $agency->phone }}
              </p>
            </td>
          </tr>
        </tbody>
      </table>
    </header>
    <center><small>Periode {{ $start }} - {{ $end }}</small></center>
    <br>
    <table class="report">
      <thead>
        <tr>
          <th class="text-center" width="10%">NIP</th>
          <th class="text-center" width="19%">Nama</th>
          <th class="text-center" width="25%">Eselon/Golongan</th>
          <th class="text-center" width="12%">TPP Awal</th>
          <th class="text-center" width="12%">Potongan</th>
          <th class="text-center" width="12%">TPP Akhir</th>
          <th class="text-center">% Kehadiran</th>
        </tr>
      </thead>
      <tbody>
        @foreach($report_data as $data)
          <tr>
            <td>{{ $data['asn']->id }}</td>
            <td>{{ $data['asn']->name }}</td>
            <td class="text-center">
              {{ substr($data['asn']->echelon->name, 0, 35) . '...' }}<br>
              {{ $data['asn']->rank->name }}
            </td>
            <td class="text-right">{{ $data['tpp']['initial_tpp'] }}</td>
            <td class="text-right">{{ $data['tpp']['deduction'] }}</td>
            <td class="text-right">{{ $data['tpp']['final_tpp'] }}</td>
            <td class="text-right">{{ $data['tpp']['attendance_rate'] }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="footer" align="right">
      {{ date('j F Y') }}
      <br>
      <br>
      <br>
      <br>
      <br>
      <p>{{ $agency->head->name }}</p>
      <p>{{ $agency->head->asn->name }}</p>
    </div>
  </body>
</html>