<!DOCTYPE html>
<html>
  <head>
    <title>Laporan TPP ASN</title>
    <style>
      body {
        font-family: sans-serif;
        font-size: 10pt;
      }

      header {
        border-bottom: 2px solid #000;
        margin-bottom: 15px;
        padding-bottom: 10px;
      }

      header img {
        height: 100px;
      }

      header h1 {
        font-size: 1.6rem;
        line-height: 1.6rem;
        margin: 0 0 5px 0;
        text-transform: uppercase;
      }

      header p {
        margin: 0 0 5px 0;
      }

      table.report {
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
      }

      table.report thead tr th,
      table.report tbody tr td,
      table.report tfoot tr td, {
        border: 1px solid #000;
        padding: 2.5px;
      }

      .footer p {
        margin-bottom: -5px;
      }

      .text-center {
        text-align: center;
      }

      .text-right {
        text-align: right;
      }

      .summary {
        padding: 5px !important;
      }
    </style>
  </head>
  <body>
    <header>
      <table width="100%">
        <tbody>
          <tr>
            <td width="1%">
              <img src="{{ resource_path('/img/bali.png') }}">
            </td>
            <td>
              <h1 align="center">Pemerintah Provinsi Bali</h1>
              <h1 align="center">{{ $asn->agency->name }}</h1>
              <p align="center">
                {{ $asn->agency->address }}
              </p>
              <p align="center">
                {{ $asn->agency->phone }}
              </p>
            </td>
          </tr>
        </tbody>
      </table>
    </header>
    <center><small>Periode {{ $start }} - {{ $end }}</small></center>
    <br>
    <table width="100%">
      <tbody>
        <tr>
          <th width="10%">NIP</th>
          <td width="25%">{{ $asn->id }}</td>
          <th width="20%">OPD</th>
          <td>{{ $asn->agency->name }}</td> 
        </tr>
        <tr>
          <th>Nama</th>
          <td>{{ $asn->name }}</td>
          <th>Eselon/Golongan</th>
          <td>{{ $asn->echelon->name }}/{{ $asn->rank->name }}</td>
        </tr>
      </tbody>
    </table>
    <br>
    <table class="report">
      <tbody>
        <thead>
        <tr>
          <th class="text-center" width="15%">Tanggal</th>
          <th class="text-center" width="15%">Masuk</th>
          <th class="text-center" width="15%">Pulang</th>
          <th class="text-center" width="10%">Status</th>
          <th class="text-center">Info</th>
          <th class="text-center" width="20%">Potongan</th>
        </tr>
      </thead>
        @foreach($report_data as $data)
          <tr>
            <td class="text-center">{{ $data['date'] }}</td>
            <td class="text-center">{{ substr($data['check_in'], 0, 5) }}</td>
            <td class="text-center">{{ substr($data['check_out'], 0, 5) }}</td>
            <td class="text-center">{{ $data['type']['id'] }}</td>
            <td>{{ $data['info'] }}</td>
            <td class="text-right">{{ $data['deduction'] }}</td>
          </tr>
        @endforeach
      </tbody>
      <tfoot>
        <tr>
          <td class="summary text-right" colspan="5">Total Potongan</td>
          <td class="summary text-right"><strong>{{ $total_deduction }}</strong></td>
        </tr>
        <tr>
          <td class="summary text-right" colspan="5">TPP Awal</td>
          <td class="summary text-right"><strong>{{ $initial_tpp }}</strong></td>
        </tr>
        <tr>
          <td class="summary text-right" colspan="5">TPP Akhir</td>
          <td class="summary text-right"><strong>{{ $final_tpp }}</strong></td>
        </tr>
      </tfoot>
    </table>
    <p style="font-size: 8pt">
      Keterangan: {{ $attendance_types }}
    </p>
    <br>
    <div class="footer" align="right">
      {{ date('j F Y') }}
      <br>
      <br>
      <br>
      <br>
      <br>
      <p>{{ $asn->agency->head->name }}</p>
      <p>{{ $asn->agency->head->asn->name }}</p>
    </div>
  </body>
</html>