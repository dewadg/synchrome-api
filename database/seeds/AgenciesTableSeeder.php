<?php

use App\Agency;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = fopen(storage_path('tsv/agencies.tsv'), 'r');
        $i = 0;

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        
        while ($data = fgetcsv($file, 0, "\t")) {
            if ($i === 0) {
                $i++;
                continue;
            }

            $found = Agency::find($data[0]);

            if (is_null($found)) {
                Agency::create([
                    'id' => $data[0],
                    'head_echelon_id' => $data[1],
                    'name' => $data[2],
                    'phone' => $data[3],
                    'address' => $data[4],
                ]);
            } else {
                $found->update([
                    'head_echelon_id' => $data[1],
                    'name' => $data[2],
                    'phone' => $data[3],
                    'address' => $data[4],
                ]);
            }

            $i++;
        }
    }
}
