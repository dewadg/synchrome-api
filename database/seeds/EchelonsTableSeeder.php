<?php

use App\Echelon;
use Illuminate\Database\Seeder;

class EchelonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = fopen(storage_path('tsv/echelons.tsv'), 'r');
        $i = 0;

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        while ($data = fgetcsv($file, 0, "\t")) {
            if ($i === 0) {
                $i++;
                continue;
            }

            $found = Echelon::find($data[0]);

            if (is_null($found)) {
                Echelon::create([
                    'id' => $data[0],
                    'supervisor_id' => $data[1],
                    'name' => $data[2],
                    'echelon_type_id' => $data[3],
                ]);
            } else {
                $found->update([
                    'supervisor_id' => $data[1],
                    'name' => $data[2],
                    'echelon_type_id' => $data[3],
                ]);
            }
        }
    }
}
