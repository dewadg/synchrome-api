<?php

use App\Asn;
use Illuminate\Database\Seeder;

class AsnTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = fopen(storage_path('tsv/asn.tsv'), 'r');
        $i = 0;

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        while ($data = fgetcsv($file, 0, "\t")) {
            if ($i === 0) {
                $i++;
                continue;
            }

            $found = Asn::find($data[0]);

            if (is_null($found)) {
                Asn::create([
                    'id' => $data[0],
                    'agency_id' => $data[1],
                    'rank_id' => $data[2],
                    'echelon_id' => $data[3],
                    'tpp_id' => $data[4],
                    'workshift_id' => $data[5],
                    'calendar_id' => $data[6],
                    'name' => $data[7],
                    'phone' => $data[8],
                    'address' => $data[9],
                    'pin' => $data[10],
                ]);
            } else {
                $found->update([
                    'agency_id' => $data[1],
                    'rank_id' => $data[2],
                    'echelon_id' => $data[3],
                    'tpp_id' => $data[4],
                    'workshift_id' => $data[5],
                    'calendar_id' => $data[6],
                    'name' => $data[7],
                    'phone' => $data[8],
                    'address' => $data[9],
                    'pin' => $data[10],
                ]);
            }

            $i++;
        }
    }
}
