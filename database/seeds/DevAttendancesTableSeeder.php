<?php

use Illuminate\Database\Seeder;

use App\Attendance;
use Carbon\Carbon;
use App\Asn;
use Illuminate\Support\Facades\DB;

class DevAttendancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $asn_list = Asn::with([
            'echelon',
            'echelon.type',
        ])->get();

        DB::table('attendances')->truncate();

        $asn_list->each(function ($item) {
            switch ($item->echelon->type->id) {
                case '2a':
                    $file = fopen(storage_path('tsv/attendances_2a.tsv'), 'r');
                    $this->generateSeeds($item, $file);
                    break;
                case '2b':
                case '3a':
                    $file = fopen(storage_path('tsv/attendances_3a.tsv'), 'r');
                    $this->generateSeeds($item, $file);
                    break;
                default:
                    echo 'Not found';
            }
        });
    }

    private function generateSeeds($asn, $file)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $i = 0;

        while ($data = fgetcsv($file, 0, "\t")) {
            if ($i === 0) {
                $i++;
                continue;
            }

            Attendance::create([
                'asn_id' => $asn->id,
                'date' => $data[0],
                'check_in' => $data[1] ? $data[1] : null,
                'check_out' => $data[2] ? $data[2] : null,
                'delay' => intval($data[3]),
                'attendance_type_id' => $data[4],
                'info' => $data[5],
            ]);

            $i++;
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
