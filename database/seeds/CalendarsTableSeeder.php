<?php

use App\Calendar;
use App\CalendarEvent;
use Illuminate\Database\Seeder;

class CalendarsTableSeeder extends Seeder
{
    private $data = [
        [
            'id' => 1,
            'name' => 'Kalender Kerja I',
            'start' => '2019-01-01',
            'end' => '2019-12-31',
            'published' => true,
            'events' => [
                [
                    'title' => 'Tahun Baru 2019',
                    'start' => '2019-01-01',
                    'attendance_type_id' => 'L',
                ],
                [
                    'title' => 'Contoh Hari Libur I',
                    'start' => '2019-06-25',
                    'attendance_type_id' => 'L',
                ],
                [
                    'title' => 'Contoh Hari Libur II',
                    'start' => '2019-07-10',
                    'attendance_type_id' => 'L',
                ],
                [
                    'title' => 'Contoh Hari Libur III',
                    'start' => '2019-07-17',
                    'attendance_type_id' => 'L',
                ],
            ],
        ],
        [
            'id' => 2,
            'name' => 'Kalender Kerja II',
            'start' => '2019-01-01',
            'end' => '2019-12-31',
            'published' => true,
            'events' => [
                [
                    'title' => 'Tahun Baru 2019',
                    'start' => '2019-01-01',
                    'attendance_type_id' => 'L',
                ],
                [
                    'title' => 'Contoh Hari Libur I',
                    'start' => '2019-06-25',
                    'attendance_type_id' => 'L',
                ],
                [
                    'title' => 'Contoh Hari Libur II',
                    'start' => '2019-07-10',
                    'attendance_type_id' => 'L',
                ],
            ],
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $data) {
            if (is_null(Calendar::find($data['id']))) {
                $calendar = Calendar::create([
                    'id' => $data['id'],
                    'name' => $data['name'],
                    'start' => $data['start'],
                    'end' => $data['end'],
                    'published' => $data['published'],
                ]);

                $events = collect($data['events'])
                    ->map(function ($item) {
                        return new CalendarEvent([
                            'title' => $item['title'],
                            'start' => $item['start'],
                            'attendance_type_id' => $item['attendance_type_id'],
                        ]);
                    });

                $calendar->events()->saveMany($events);
            }
        }
    }
}
